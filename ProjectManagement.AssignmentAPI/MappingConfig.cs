﻿

using AutoMapper;
using ProjectManagemen.ProjectAPI.Models;
using TaskAPI.Models.Dtos;

namespace ProjectManagemen.AssigmentAPI
{
    public class MappingConfig
    {
        public static MapperConfiguration RegisterMap()
        {
            var mappingConfig = new MapperConfiguration(config =>
            {
                config.CreateMap<AssigmentModel, AssigmentDto>().ReverseMap();
            });
            return mappingConfig;
        }
    }
}
