﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace ProjectManagemen.ProjectAPI.Models
{
    public class AssigmentModel
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int TaskId { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public DateTime AssignedDate { get; set; }
        [Required]
        public DateTime DueDate { get; set; }

    }
}
