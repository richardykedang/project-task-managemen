﻿using System.ComponentModel.DataAnnotations;

namespace TaskAPI.Models.Dtos
{
    public class AssigmentDto
    {   
        public int Id { get; set; }
        public int TaskId { get; set; }
        public int UserId { get; set; }
        public DateTime AssignedDate { get; set; }
        public DateTime DueDate { get; set; }
    }
}
