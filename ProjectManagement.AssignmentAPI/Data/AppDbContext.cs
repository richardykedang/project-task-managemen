﻿
using Microsoft.EntityFrameworkCore;
using ProjectManagemen.ProjectAPI.Models;

namespace ProjectManagemen.AssigmentAPI.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        public DbSet<AssigmentModel> Assigments { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //modelBuilder.Entity<AssigmentModel>().HasData(new AssigmentModel
            //{
            //    Id = 1,
            //    TaskId = 1,
            //    UserId = 1,
            //    AssignedDate = new DateTime(2024 - 07 - 01),
            //    DueDate = new DateTime(2024 - 07 - 15)
            //}) ;
        }
    }
}
