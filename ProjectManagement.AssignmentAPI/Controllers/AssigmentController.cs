﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectManagemen.AssigmentAPI.Data;
using ProjectManagemen.AssigmentAPI.Models.Dtos;
using ProjectManagemen.AssigmentAPI.Service.IService;
using ProjectManagemen.ProjectAPI.Models;
using TaskAPI.Models.Dtos;

namespace ProjectManagement.AssignmentAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AssigmentController : ControllerBase
    {
        private readonly AppDbContext _db;
        private ResponseDto _response;
        private IMapper _mapper;
        private readonly ITaskService _taskService;

        public AssigmentController(AppDbContext db, IMapper mapper, ITaskService taskService)
        {
            _db = db;
            _response = new ResponseDto();
            _mapper = mapper;
            _taskService = taskService;

        }

        [HttpGet]

        public async Task<ResponseDto> Get()
        {

            try
            {
                IEnumerable<AssigmentModel> taskList = _db.Assigments.ToList();

                _response.Result = _mapper.Map<IEnumerable<AssigmentDto>>(taskList);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }
            return _response;
        }

        [HttpGet]
        [Route("{id:int}")]
        public ResponseDto Get(int id)
        {

            try
            {
                var assignment = _db.Assigments.FirstOrDefault(a => a.Id == id);
                if (assignment == null) throw new Exception("Assignment not found");
                _response.Result = assignment;

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Result = ex.Message.ToString();
            }
            return _response;
        }

        [HttpGet("GetByTaskId/{taskId:int}")]
        public ResponseDto GetAssignmentsByTaskId(int taskId)
        {
            try
            {
                var assignments = _db.Assigments.Where(a => a.TaskId == taskId).ToList();
                _response.Result = assignments;
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Result = ex.Message;
            }
            return _response;
        }

        [HttpGet("GetByUserId/{userId:int}")]
        public ResponseDto GetAssignmentsByUserId(int userId)
        {
            try
            {
                var assignments = _db.Assigments.Where(a => a.UserId == userId).ToList();
                _response.Result = assignments;
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Result = ex.Message;
            }
            return _response;
        }

        [HttpPost]
        public async Task<ResponseDto> CreateAssignment(AssigmentDto assignmentDto)
        {
            try
            {
                var GetTaskId = await _taskService.GetTasks();
                var TasktExist = GetTaskId.FirstOrDefault(t => t.TaskId == assignmentDto.TaskId);
                if (TasktExist == null)
                {
                    _response.IsSuccess = false;
                    _response.Message = "Project Not Found";
                    return _response;
                }
                var assigment = _mapper.Map<AssigmentModel>(assignmentDto);
                
                _db.Assigments.Add(assigment);
                _db.SaveChanges();

                assignmentDto.Id = assigment.Id;
                _response.Result = assignmentDto;
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Result = ex.Message;
            }
            return _response;
        }

        [HttpPut]
        public async Task<ResponseDto> UpdateAssignment(AssigmentDto assignmentDto)
        {
            try
            {
                var assignment = _db.Assigments.FirstOrDefault(a => a.Id == assignmentDto.Id);
                if (assignment == null) throw new Exception("Assignment not found");

                var GetTaskId = await _taskService.GetTasks();
                var TasktExist = GetTaskId.FirstOrDefault(t => t.TaskId == assignmentDto.TaskId);
                if (TasktExist == null)
                {
                    _response.IsSuccess = false;
                    _response.Message = "Project Not Found";
                    return _response;
                }

                assignment.TaskId = assignmentDto.TaskId;
                assignment.UserId = assignmentDto.UserId;
                assignment.AssignedDate = assignmentDto.AssignedDate;
                assignment.DueDate = assignmentDto.DueDate;

                _db.Assigments.Update(assignment);
                _db.SaveChanges();

                _response.Result = assignmentDto;
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Result = ex.Message;
            }
            return _response;
        }

        [HttpDelete("{id:int}")]
        public async Task<ResponseDto> DeleteAssignment(int id)
        {
            try
            {
                var assignment = _db.Assigments.FirstOrDefault(a => a.Id == id);
                if (assignment == null) throw new Exception("Assignment not found");

                _db.Assigments.Remove(assignment);
                _db.SaveChanges();

                _response.Result = "Assignment deleted successfully";
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Result = ex.Message;
            }
            return _response;
        }
    }
}
