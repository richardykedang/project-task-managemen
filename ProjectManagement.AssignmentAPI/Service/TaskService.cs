﻿using Newtonsoft.Json;
using ProjectManagemen.AssigmentAPI.Models.Dtos;
using ProjectManagemen.AssigmentAPI.Service.IService;
using ProjectManagemen.ProjectAPI.Models.Dtos;

namespace ProjectManagemen.AssigmentAPI.Service
{
    public class TaskService : ITaskService
    {
        private readonly IHttpClientFactory _httpClientFactory;
        

        public TaskService(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
            
        }
        public async Task<IEnumerable<TaskDto>> GetTasks()
        {
            var client = _httpClientFactory.CreateClient("Task");
            var response = await client.GetAsync($"/api/TaskAPI/GetAllTask");
            var apiContent = await response.Content.ReadAsStringAsync();
            var resp = JsonConvert.DeserializeObject<ResponseDto>(apiContent);
            if (resp.IsSuccess)
            {
                return JsonConvert.DeserializeObject<IEnumerable<TaskDto>>(Convert.ToString(resp.Result));
            }
            return new List<TaskDto>();
        }

    }
}
