﻿using ProjectManagemen.ProjectAPI.Models.Dtos;

namespace ProjectManagemen.AssigmentAPI.Service.IService
{
    public interface ITaskService
    {
        Task<IEnumerable<TaskDto>> GetTasks();
    }
}
