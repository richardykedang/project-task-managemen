﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace ProjectManagemen.ProjectAPI.Models
{
    public class TaskModel
    {
        [Key]
        public int TaskId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public int ProjectHeaderId { get; set; }
        [Required]
        public DateTime DueDate { get; set; }

        
    }
}
