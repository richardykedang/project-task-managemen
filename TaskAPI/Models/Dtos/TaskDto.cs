﻿using System.ComponentModel.DataAnnotations;

namespace TaskAPI.Models.Dtos
{
    public class TaskDto
    {
        
        public int TaskId { get; set; }
        
        public string Name { get; set; }
  
        public string Description { get; set; }
      
        public int ProjectHeaderId { get; set; }
   
        public DateTime DueDate { get; set; }
    }
}
