﻿
using System.ComponentModel.DataAnnotations;

namespace ProjectManagemen.ProjectAPI.Models.Dtos
{
    public class ProjectDto
    {
        public int ProjectId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
