﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjectManagemen.ProjectAPI.Models;
using ProjectManagemen.ProjectAPI.Models.Dtos;
using ProjectManagemen.TaskAPI.Data;
using ProjectManagemen.TaskAPI.Models.Dtos;
using System.Threading.Tasks;
using TaskAPI.Models.Dtos;
using TaskAPI.Service.IService;

namespace TaskAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TaskAPIController : ControllerBase
    {
        private readonly AppDbContext _db;
        private ResponseDto _response;
        private IMapper _mapper;
        private readonly IProjectService _projectService;

        public TaskAPIController(AppDbContext db, IMapper mapper, IProjectService projectService)
        {
            _db = db;
            _mapper = mapper;
            _projectService = projectService;
            _response = new ResponseDto();
        }

        [HttpGet]
        [Route("GetAllTask")]
        public async Task<ResponseDto> GetAllTask()
        {

            try
            {
                IEnumerable<TaskModel> taskList = _db.Tasks.ToList();

                _response.Result = _mapper.Map<IEnumerable<TaskDto>>(taskList);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }
            return _response;
        }

        [HttpPost]
        [Authorize(Roles = "ADMIN")]
        public async Task<ResponseDto> CreateTask(TaskDto model)
        {
            try
            {
                // Check ProjectId on ProjectAPI 
             
                var Project = await _projectService.GetProjects();
                var ProjectExist = Project.FirstOrDefault(p => p.ProjectId == model.ProjectHeaderId);
                if (ProjectExist == null)
                {
                    _response.IsSuccess = false;
                    _response.Message = "Project Not Found";
                }
                else
                {
                    if (model.DueDate > ProjectExist.EndDate)
                    {
                        _response.IsSuccess = false;
                        _response.Message = "Task DueDate cannot be later than Project EndDate";
                        return _response;
                    }
                    var TaskObj = _mapper.Map<TaskModel>(model);
                    _db.Add(TaskObj);
                    await _db.SaveChangesAsync();
                    _response.Message = "Success Adding to Database!";
                    _response.Result = TaskObj;
                    _response.IsSuccess = true;
                }

                

                
            }
            catch (Exception ex)
            {

                _response.IsSuccess = false;
                _response.Result = ex.Message.ToString();
            }
            return _response;
        }

        [HttpPut]
        [Authorize(Roles = "ADMIN")]
        public async Task<ResponseDto> UpdateTask(TaskDto model)
        {
            try
            {
                // Check task Id exist
                var task = _db.Tasks.AsNoTracking().FirstOrDefault(t => t.TaskId == model.TaskId);
                if (task == null) throw new Exception("Task not found");

                var Project = await _projectService.GetProjects();
                var ProjectExist = Project.FirstOrDefault(p => p.ProjectId == model.ProjectHeaderId);
                if (ProjectExist == null)
                {
                    _response.IsSuccess = false;
                    _response.Message = "Project Not Found";
                }
                else
                {
                    if (model.DueDate > ProjectExist.EndDate)
                    {
                        _response.IsSuccess = false;
                        _response.Message = "Task DueDate cannot be later than Project EndDate";
                        return _response;
                    }
                    var TaskObj = _mapper.Map<TaskModel>(model);
                    _db.Update(TaskObj);
                    await _db.SaveChangesAsync();
                    _response.Message = "Success Update to Database!";
                    _response.Result = TaskObj;
                    _response.IsSuccess = true;
                }




            }
            catch (Exception ex)
            {

                _response.IsSuccess = false;
                _response.Result = ex.Message.ToString();
            }
            return _response;
        }



        [HttpGet]
        [Route("TestingGetProductAPI")]
        public async Task<ResponseDto> TestingGetProductAPI()
        {
            var Project = await _projectService.GetProjects();
            _response.Result = Project;
            return _response;
        }
    }
}
