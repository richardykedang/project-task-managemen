﻿using AutoMapper;
using ProjectManagemen.ProjectAPI.Models;
using TaskAPI.Models.Dtos;

namespace ProjectManagemen.TaskAPI
{
    public class MappingConfig
    {
        public static MapperConfiguration RegisterMap()
        {
            var mappingConfig = new MapperConfiguration(config =>
            {
                config.CreateMap<TaskModel, TaskDto>().ReverseMap();
            });
            return mappingConfig;
        }
    }
}
