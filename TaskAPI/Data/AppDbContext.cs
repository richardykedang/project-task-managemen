﻿
using Microsoft.EntityFrameworkCore;
using ProjectManagemen.ProjectAPI.Models;

namespace ProjectManagemen.TaskAPI.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        public DbSet<TaskModel> Tasks { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<TaskModel>().HasData(new TaskModel
            {
                TaskId = 1,
                Name = "Sample Task",
                Description = "This is a sample task",
                ProjectHeaderId = 1,
                DueDate = new DateTime(2024, 07, 15)
            });
        }
    }
}
