﻿using ProjectManagemen.ProjectAPI.Models.Dtos;

namespace TaskAPI.Service.IService
{
    public interface IProjectService
    {
        Task<IEnumerable<ProjectDto>> GetProjects();
    }
}
