﻿using Newtonsoft.Json;
using ProjectManagemen.ProjectAPI.Models.Dtos;
using ProjectManagemen.TaskAPI.Models.Dtos;
using System.Net.Http.Headers;
using TaskAPI.Service.IService;


namespace TaskAPI.Service
{
    public class ProjectService : IProjectService
    {
        private readonly IHttpClientFactory _httpClientFactory;
        

        public ProjectService(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
            
        }
        public async Task<IEnumerable<ProjectDto>> GetProjects()
        {
            var client = _httpClientFactory.CreateClient("Project");
            var response = await client.GetAsync($"/api/ProjectAPI/GetAll");
            var apiContent = await response.Content.ReadAsStringAsync();
            var resp = JsonConvert.DeserializeObject<ResponseDto>(apiContent);
            if (resp.IsSuccess)
            {
                return JsonConvert.DeserializeObject<IEnumerable<ProjectDto>>(Convert.ToString(resp.Result));
            }
            return new List<ProjectDto>();
        }

    }
}
