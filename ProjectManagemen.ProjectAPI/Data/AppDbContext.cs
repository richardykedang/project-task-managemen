﻿using Microsoft.EntityFrameworkCore;
using ProjectManagemen.ProjectAPI.Models;
using ProjectManagemen.ProjectAPI.Models.Dtos;
using static Azure.Core.HttpHeader;

namespace ProjectManagemen.ProjectAPI.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        public DbSet<Project> Projects { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Project>().HasData(new Project
            {
                ProjectId = 1,
                Name = "Project1",
                Description = "Project1 Description",
                StartDate = DateTime.Now,
                EndDate = new DateTime(2024, 12, 31)
            });
        }
    }
}
