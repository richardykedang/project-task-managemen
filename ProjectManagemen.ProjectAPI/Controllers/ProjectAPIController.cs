﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectManagemen.ProjectAPI.Data;
using ProjectManagemen.ProjectAPI.Models;
using ProjectManagemen.ProjectAPI.Models.Dtos;
using ProjectManagemen.ProjectAPI.Utility;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace ProjectManagemen.ProjectAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ProjectAPIController : ControllerBase
    {
        private readonly AppDbContext _db;
        private readonly ResponseDto _response;
        private IMapper _mapper;

        public ProjectAPIController(AppDbContext db, IMapper mapper)
        {
            _db = db;
            _response = new ResponseDto();
            _mapper = mapper;
        }

        [HttpGet]
        [Route("GetAll")]
        public ResponseDto GetAll()
        {

            try
            {
                IEnumerable<Project> projectsList = _db.Projects.ToList();
                #region// Manual Mapper

                //List<ProjectDto> projectDtos = new List<ProjectDto>();
                //foreach (var item in projectsList)
                //{
                //    ProjectDto projectDto= new ProjectDto()
                //    {
                //        ProjectId = item.ProjectId, 
                //        Name = item.Name,
                //    };
                //    projectDtos.Add(projectDto);
                //}
                #endregion 

                _response.Result = _mapper.Map<IEnumerable<ProjectDto>>(projectsList);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }
            return _response;
        }

        [HttpGet]
        [Route("{id:int}")]
        public ResponseDto Get(int id)
        {

            try
            {
                Project project = _db.Projects.First(p => p.ProjectId == id);
                #region ManualMapper
                //ProjectDto projectObj = new ProjectDto()
                //{
                //    ProjectId = project.ProjectId,
                //    Name = project.Name,
                //};
                #endregion
                _response.Result = _mapper.Map<ProjectDto>(project);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Result = ex.Message.ToString();
            }
            return _response;
        }

        [HttpGet]
        [Route("GetByName/{name}")]
        public ResponseDto GetByName(string name)
        {

            try
            {
                // Just return One Project
                //Project project = _db.Projects.First(p => p.Name == name);

                // Return List Project
                IEnumerable<Project>? listProjects = _db.Projects.Where(p => p.Name.Contains(name)).ToList();

                //_response.Result = _mapper.Map<ProjectDto>(project);
                _response.Result = _mapper.Map<IEnumerable<ProjectDto>>(listProjects);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Result = ex.Message.ToString();
            }
            return _response;
        }

        [HttpPost]
        [Authorize(Roles ="ADMIN")]
        public ResponseDto Post([FromBody] ProjectDto model)
        {

            try
            {
                #region Manual Mapper
                //Project project = new Project()
                //{
                //    Name = model.Name,
                //    Description = model.Description,
                //    StartDate = model.StartDate,
                //    EndDate = model.EndDate,
                //};
                #endregion
                
                bool ProjectExist = _db.Projects.Any(p => p.Name.ToLower() == model.Name.ToLower());
                if (!ProjectExist)
                {
                    Project project = _mapper.Map<Project>(model);
                    _db.Add(project);
                    _db.SaveChanges();

                    _response.Message = SD.Succes_Adding;
                    _response.Result = project;
                }
                else
                {
                    _response.IsSuccess = false;
                    _response.Message = "Project Already Exist";
                }
            }

            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Result = ex.Message.ToString();
            }
            return _response;
        }

        [HttpPut]
        [Authorize(Roles = "ADMIN")]
        public ResponseDto Put([FromBody] ProjectDto model)
        {

            try
            {
                #region Manual Mapper
                //Project project = new Project()
                //{
                //    Name = model.Name,
                //    Description = model.Description,
                //    StartDate = model.StartDate,
                //    EndDate = model.EndDate,
                //};
                #endregion

                Project project = _mapper.Map<Project>(model);
                _db.Update(project);
                _db.SaveChanges();

                _response.Message = SD.Success_Update;
                _response.Result = project;

            }

            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Result = ex.Message.ToString();
            }
            return _response;
        }

        [HttpDelete]
        [Route("{id:int}")]
        [Authorize(Roles = "ADMIN")]
        public ResponseDto Delete(int id)
        {

            try
            {
                Project projectObj = _db.Projects.First(x => x.ProjectId == id);
                Project project = _mapper.Map<Project>(projectObj);
                _db.Remove(project);
                _db.SaveChanges();

                _response.Message = SD.Success_Remove;
                _response.Result = project;

            }

            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Result = ex.Message.ToString();
            }
            return _response;
        }
    }
}
