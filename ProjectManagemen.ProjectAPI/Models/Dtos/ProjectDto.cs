﻿
using System.ComponentModel.DataAnnotations;

namespace ProjectManagemen.ProjectAPI.Models.Dtos
{
    public class ProjectDto
    {
        public int ProjectId { get; set; }
        //[Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        public string Description { get; set; }
        //[Required(ErrorMessage = "StartDate is required")]
        public DateTime StartDate { get; set; }

        [Compare(nameof(StartDate), ErrorMessage = "End Date cannot be earlier than Start Date")]
        public DateTime EndDate { get; set; }
    }
}
