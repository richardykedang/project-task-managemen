﻿using AutoMapper;
using ProjectManagemen.ProjectAPI.Models;
using ProjectManagemen.ProjectAPI.Models.Dtos;

namespace ProjectManagemen.ProjectAPI
{
    public class MappingConfig
    {
        public static MapperConfiguration RegisterMap()
        {
            var mappingConfig = new MapperConfiguration(config =>
            {
                config.CreateMap<ProjectDto, Project>().ReverseMap();
            });
            return mappingConfig;
        }
    }
}
