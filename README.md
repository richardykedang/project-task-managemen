# Sistem Manajemen Tugas Proyek

Repository ini berisi sistem manajemen tugas proyek berbasis microservice, dirancang untuk mengelola proyek, tugas, penugasan, dan otentikasi pengguna.

## Gambaran Umum Microservices

### ProjectAPI

- **Model**:
  - `Project`: Mewakili sebuah proyek dengan atribut seperti ProjectId, Name, Description, StartDate, dan EndDate.

- **Metode**:
  - `GET /api/ProjectAPI/GetAll`: Mengambil semua proyek.
  - `GET /api/ProjectAPI/{id}`: Mengambil proyek berdasarkan ID.
  - `GET /api/ProjectAPI/GetByName/{name}`: Mengambil proyek berdasarkan nama.
  - `POST /api/ProjectAPI/CreateProject`: Membuat proyek baru.
  - `PUT /api/ProjectAPI/UpdateProject/{id}`: Memperbarui proyek yang ada.
  - `DELETE /api/ProjectAPI/DeleteProject/{id}`: Menghapus proyek.

### TaskAPI

- **Model**:
  - `Task`: Mewakili sebuah tugas yang terkait dengan proyek, dengan atribut seperti TaskId, Name, Description, ProjectId, dan DueDate.

- **Metode**:
  - `POST /api/TaskAPI/CreateTask`: Membuat tugas baru.
  - `GET /api/TaskAPI/GetAllTask`: Mengambil semua tugas.
  - `PUT /api/TaskAPI/UpdateTask/{id}`: Memperbarui tugas yang ada.

### AssignmentAPI

- **Model**:
  - `Assignment`: Mewakili penugasan suatu tugas kepada seorang pengguna, dengan atribut seperti Id, TaskId, UserId, AssignedDate, dan DueDate.

- **Metode**:
  - `GET /api/AssignmentAPI/GetAllAssignment`: Mengambil semua penugasan.
  - `GET /api/AssignmentAPI/GetAssignmentById/{id}`: Mengambil penugasan berdasarkan ID.
  - `GET /api/AssignmentAPI/GetAssignmentByTaskId/{taskId}`: Mengambil penugasan berdasarkan ID tugas.
  - `GET /api/AssignmentAPI/GetAssignmentByUserId/{userId}`: Mengambil penugasan berdasarkan ID pengguna.
  - `POST /api/AssignmentAPI/CreateAssignment`: Membuat penugasan baru.
  - `PUT /api/AssignmentAPI/UpdateAssignment/{id}`: Memperbarui penugasan yang ada.
  - `DELETE /api/AssignmentAPI/DeleteAssignment/{id}`: Menghapus penugasan.

### AuthAPI

- **Metode**:
  - `POST /api/AuthAPI/Register`: Mendaftarkan pengguna baru.
  - `POST /api/AuthAPI/Login`: Mengotentikasi dan masuk ke sistem.
  - `POST /api/AuthAPI/AssignRole`: Memberikan peran kepada pengguna.

## Teknologi yang Digunakan

- **.NET Core 6**: Framework untuk membangun API.
- **Entity Framework Core**: ORM untuk operasi database.
- **ASP.NET Core Web API**: Membangun layanan RESTful.
- **Swagger/OpenAPI**: Dokumentasi API.
- **JSON Web Token (JWT)**: Untuk otentikasi dan otorisasi pengguna.

