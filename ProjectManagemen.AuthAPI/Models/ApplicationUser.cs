﻿using Microsoft.AspNetCore.Identity;

namespace ProjectManagemen.AuthAPI.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string Name { get; set; }
    }
}
