﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectManagemen.AuthAPI.Models.Dto;
using ProjectManagemen.AuthAPI.Models.Dtos;
using ProjectManagemen.AuthAPI.Service.IService;

namespace ProjectManagemen.AuthAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthAPIController : ControllerBase
    {
        private readonly IAuthService _authService;
        protected ResponseDto _response;

        public AuthAPIController(IAuthService authService)
        {
            _authService = authService;
            _response = new ResponseDto();
        }


        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegistrationRequestDto model)
        {
            string errorMessage = await _authService.Register(model);
            if(!string.IsNullOrEmpty(errorMessage))
            {
                _response.Message = errorMessage;
                _response.IsSuccess = false;
                return BadRequest(_response);
            }
            return Ok(_response);
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] LoginRequestDto model)
        {
            LoginResponseDto login = await _authService.Login(model);
            if(login.User == null)
            {
                _response.IsSuccess = false;
                _response.Message = "Username or Password is Incorrect";
                return BadRequest(_response);
            }
            return Ok(login);
        }

        [HttpPost("AssignRole")]
        public async Task<IActionResult> AssignRole([FromBody] RegistrationRequestDto model)
        {
            bool assignRole = await _authService.AssignRole(model.Email, model.Role.ToUpper());
            if (!assignRole)
            {
                _response.IsSuccess=false;
                _response.Message = "Error Encountered";
                return BadRequest(_response);
            }
            return Ok(assignRole);
        }
    }
}
