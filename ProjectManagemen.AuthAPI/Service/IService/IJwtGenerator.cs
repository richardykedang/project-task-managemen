﻿using ProjectManagemen.AuthAPI.Models;

namespace ProjectManagemen.AuthAPI.Service.IService
{
    public interface IJwtGenerator
    {
        string GenerateToken(ApplicationUser applicationUser, IEnumerable<string> role);
    }
}
